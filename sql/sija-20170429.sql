-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db_sija
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_jasa`
--

DROP TABLE IF EXISTS `tbl_jasa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_jasa` (
  `jid` int(32) NOT NULL AUTO_INCREMENT,
  `nama_jasa` varchar(20) NOT NULL,
  `alamat` varchar(30) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `deskripsi_jasa` text,
  `kategori` int(11) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`jid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_jasa`
--

LOCK TABLES `tbl_jasa` WRITE;
/*!40000 ALTER TABLE `tbl_jasa` DISABLE KEYS */;
INSERT INTO `tbl_jasa` VALUES (1,'jasa1','jasa1',NULL,NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `tbl_jasa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_jasa_kategori`
--

DROP TABLE IF EXISTS `tbl_jasa_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_jasa_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_jasa_kategori`
--

LOCK TABLES `tbl_jasa_kategori` WRITE;
/*!40000 ALTER TABLE `tbl_jasa_kategori` DISABLE KEYS */;
INSERT INTO `tbl_jasa_kategori` VALUES (1,'Servis AC'),(2,'Bimbingan Belajar'),(3,'Laundry');
/*!40000 ALTER TABLE `tbl_jasa_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_member`
--

DROP TABLE IF EXISTS `tbl_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_member` (
  `uid` int(32) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  `kontak` varchar(15) NOT NULL,
  `email` varchar(20) NOT NULL,
  `password` varchar(8) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_member`
--

LOCK TABLES `tbl_member` WRITE;
/*!40000 ALTER TABLE `tbl_member` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_review`
--

DROP TABLE IF EXISTS `tbl_review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_review` (
  `uid` int(32) NOT NULL,
  `jid` int(32) NOT NULL,
  `review` varchar(250) NOT NULL,
  `rate` int(32) NOT NULL,
  KEY `uid` (`uid`),
  KEY `jid` (`jid`),
  CONSTRAINT `tbl_review_ibfk_1` FOREIGN KEY (`uid`) REFERENCES `tbl_member` (`uid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_review_ibfk_2` FOREIGN KEY (`jid`) REFERENCES `tbl_jasa` (`jid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_review`
--

LOCK TABLES `tbl_review` WRITE;
/*!40000 ALTER TABLE `tbl_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(23) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(20) NOT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `unique_id` (`unique_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'58d487446e6049.97063752','nasrul','nasrul@tes.com',1234567,'4hpKDvtXnRgsirDoaknaGMf5pvc1ZGI0NjVhYTll','5db465aa9e','2017-03-24 09:41:08',NULL),(2,'58d4bbf7900ce4.61378773','tes','tes@tes.com',1234,'zzj44SOLD2lRpdErgpEduONjg30yODlkMDA5Nzg2','289d009786','2017-03-24 13:25:59',NULL),(9,'58d4bc68e891f8.31336925','tes','tes2@tes.com',12345,'ZNvHqnUhHbGo2rC8Qw+FZnKgvKY1MWRhZWViNTBl','51daeeb50e','2017-03-24 13:27:52',NULL),(10,'58d5b308b6d0f4.39563632','tes3','tes3@tes.com',1234564890,'jDoUNsaIiN+CY7IdtWSgRDHCzmZlOTY4ZWRhZTE1','e968edae15','2017-03-25 07:00:08',NULL),(11,'58d60bd4badaa4.72379148','bagus','bagus@tes.com',1234567890,'RXELybAa1ErSYxmKdb8kgis0sl4zYjUzMDU0MWFl','3b530541ae','2017-03-25 13:19:00',NULL),(12,'58d7f28b7dafc7.98784748','tes4','tes4@tes.com',78487234,'DDAHHpI0uCF7CQ54TbzaVOJA2eI1ZmQ2YjdiMjcz','5fd6b7b273','2017-03-26 23:55:39',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-29 13:01:00
